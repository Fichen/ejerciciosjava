/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicio1;

/**
 *
 * @author alumno
 */
public class Auto {
            
    private enum EMarca{ Ford, Fiat, Chevrolet};
    private String patente;
    private String modelo;
    private double precio;
    
    
    public String getPatente() {
        return this.patente;
    }
    
    public void setPatente(String patente) {
        this.patente = patente;
    }
    
    public String getModelo() {
        return this.modelo;
    }
    
    public void setModelo(String modelo) {
        this.modelo = modelo;
    }
    
    public double getPrecio() {
        return this.precio;
    }
    
    public void setPrecio(double precio) throws Exception{
        
        this.precio = precio;
        
    }
    
    @Override
    public String toString() {
        String returnString = "Auto"
                + ""
                + "";
        
        return returnString;
    }
}
