/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg3_sep;

import java.util.Scanner;

/**
 *
 * @author alumno
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        //Objeto para leer la entrada del usuario
//        Scanner sc = new Scanner(System.in);
//        
//        System.out.println("Ingrese palabra: ");
        
        //Lee el dato ingreado por el usuario
//        String word1 = sc.nextLine();
//        System.out.println("Se ingresó: "+word1.toString());
        
        
        /////////////////////////////
        String algo1 = new String("Hola");
        //Idem arriba, pero java sobrecarga el constructor
        String algo2 = "Hola";
        
        //La clase String herada de Objects, por lo cual 
        //sobreescribe el metodo equals. Esto permite que 
        //comparar los objetos como si fueran cadenas de caracteres
        if (algo1.equals(algo2)) {
            System.out.println("SI");
        }
        else {
            System.out.println("NO");
        }            
        
        //Esto es equivalente al tipo primitivo de un string.
        //Es un array de char
        //char[] arraychar = algo1.toCharArray();
        
        System.out.println(algo1.length());
        //Esto no es un simple modificacion/asignacion del string
        //Sino que lo que hace la clase es generar una nueva referencia en memoria
        //Antes algo1 tenia una cadena char de 5 posiciones. Luego de esta asignación
        //Ahora va a tener 7 posiciones (la ultima es el salto de linea). Pero internamente
        //es una nueva referencia a memoria 
        algo1 = "SARASA";
        System.out.println(algo1.length());
        String stringCortado = algo1.substring(1, 4);
        System.out.println(stringCortado.toString());
        
        
        // #######################################
        // WRAPERS
        //Float = float
        //Character = char
        //Boolean = boolean
        //Long = long
        
        Integer valor1 = 1;
        Double  valor2 = new Double(2);
        Double valor3 = Double.valueOf(2.2);
        
        //El wraper permite obtener el valor primitivo de otro tipo de valor.
        //Hace el casteo desde el mismo objeto wraper hacia otro tipo primitivo
        //ya que hereda de la clase generica number
        valor1 = valor3.intValue();
        
        //valor2.
        
    }
}
