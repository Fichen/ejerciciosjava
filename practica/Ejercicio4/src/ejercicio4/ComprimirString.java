/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ejercicio4;

/**
 * Contabiliza la cantidad de caracteres que se encuentran de corrido en una cadena
 * @author dakota
 * @version 1.0
 */
public class ComprimirString {

    public static String comprimir(String cadena) {
        String nuevaCadena = "";
        char[] arrayChar = cadena.toLowerCase().toCharArray();
        
        Integer contador = 1;                
        for (int i = 0; i <= arrayChar.length - 1; i++) {                        
            if (i + 1 <= (arrayChar.length - 1)) {
                if (arrayChar[i] == arrayChar[i + 1]) {
                    contador++;
                } else {
                    nuevaCadena += arrayChar[i] + contador.toString();
                    contador = 1;
                }
            } else {
                nuevaCadena += arrayChar[i] + contador.toString();
                contador = 1;
            }

        }
        
        return nuevaCadena;
    }
}
