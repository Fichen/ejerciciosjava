/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ejercicio4;

/**
 *
 * @author dakota
 */
public class Ejercicio4 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        String cadena = "aaAaBbcccccab";
        String cadenaComprimida = "";
        
        cadenaComprimida = ComprimirString.comprimir(cadena);
        
        System.out.println("Cadena origen: "+ cadena);
        System.out.println("Cadena comprimida: "+cadenaComprimida);
    }
    
}
