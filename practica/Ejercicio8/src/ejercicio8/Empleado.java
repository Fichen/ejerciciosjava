/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicio8;

/**
 *
 * @author dakota
 */
abstract class Empleado {
    
    private String nombre;
    private Integer edad;
    private String departamento;

    public Empleado() {
    }
    
    public Empleado(String nombre, Integer edad, String departamento) {
        this.nombre = nombre;
        this.edad = edad;
        this.departamento = departamento;
    }

    public String getDepartamento() {
        return departamento;
    }

    public Integer getEdad() {
        return edad;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
       
    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public void setEdad(Integer edad) {
        this.edad = edad;
    }
    
    abstract Double calcularSueldo() throws Exception;
    
    public String toString() {
        String string = " Empleado Fijo\n"
                + "Nombre: " + this.getNombre() + "\n"
                + "Edad: " + this.getEdad() + "\n"
                + "Departamento: " +this.getDepartamento() + "\n";
        return string;
    }
}
