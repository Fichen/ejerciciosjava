/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicio8;

/**
 *
 * @author dakota
 */
public class Ejercicio8 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        EmpleadoFijo empleadoFijo = new EmpleadoFijo();
        EmpleadoTemporal empleadoTemporal = new EmpleadoTemporal();
        EmpleadoPorHoras empleadoPorHoras = new EmpleadoPorHoras();
        
        //EmpleadoFijo
        empleadoFijo.setDepartamento("Sistemas");
        empleadoFijo.setEdad(30);
        empleadoFijo.setNombre("Dave");
        empleadoFijo.setSueldoMensual(Double.valueOf(10000));
        System.out.println(empleadoFijo);
        
        //EmpleadoTemporal
        empleadoTemporal.setDepartamento("Gerencia");
        empleadoTemporal.setEdad(45);
        empleadoTemporal.setNombre("NN");
        empleadoTemporal.setFechaAlta("2004-06-02");
        //empleadoTemporal.setSueldoMensual(Double.valueOf(20000));
        System.out.println(empleadoTemporal);
    }
    
}
