/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicio8;

/**
 *
 * @author dakota
 */
public class EmpleadoFijo extends Empleado {

    private Double sueldoMensual;
    
    public EmpleadoFijo() {
        super();                                
    }

    public EmpleadoFijo(Double sueldoMensual, String nombre, Integer edad, String departamento) {
        super(nombre, edad, departamento);
        this.sueldoMensual = sueldoMensual;
    }        
    
    public Double getSueldoMensual() {
        return sueldoMensual;
    }
    
    public void setSueldoMensual(Double sueldoMensual) {
        this.sueldoMensual = sueldoMensual;
    }
 
    @Override
    public Double calcularSueldo() throws Exception {
        if(this.sueldoMensual == null) {
            throw new Exception("El empleado "+this.getNombre() + " no tiene asignado el sueldo");
        }
        
        return this.sueldoMensual;
    }
    
    @Override
    public String toString() {
        Boolean flagError = false;
        String string = super.toString();
        try {
            string += "Sueldo mensual: "+this.calcularSueldo();
            string += "\n";
            string += "####################";
        } catch(Exception ex){
            flagError = true;
        }
        finally {
            if(flagError) {
                string += "El sueldo mensual no se pudo determinar \n";
            }
        }
        
        return string;
    }
}
    