/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicio8;

/**
 *
 * @author dakota
 */
public class EmpleadoPorHoras extends Empleado {
    public final static Double valorHora = 220.0;
    
    private Integer horasTrabajadas;

    public EmpleadoPorHoras() {
        super();
    }

    public EmpleadoPorHoras(Integer horasTrabajadas, String nombre, Integer edad, String departamento) {
        super(nombre, edad, departamento);
        this.horasTrabajadas = horasTrabajadas;
    }
    
   public Integer getHorasTrabajadas() {
       return horasTrabajadas;
   }
   
   public void setHorasTrabajadas(Integer horasTrabajadas) {
       this.horasTrabajadas = horasTrabajadas;
   }
   
    @Override
   public Double calcularSueldo() throws Exception {
       if(horasTrabajadas == null) {
           throw new Exception("No se ingresaron las horas trabajadas");
       }
       
       return (horasTrabajadas * valorHora);
   }
      
}
