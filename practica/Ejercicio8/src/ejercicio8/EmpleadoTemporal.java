/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicio8;

/**
 *
 * @author dakota
 */
public class EmpleadoTemporal extends Empleado{
    
    private String fechaAlta;
    private String fechaBaja;
    private Double sueldoMensual;

    public EmpleadoTemporal() {
        super();
        this.fechaBaja = "0000-00-00";
    }
        
    public EmpleadoTemporal(String fechaAlta, String fechaBaja, Double sueldoMensual, String nombre, Integer edad, String departamento) {
        super(nombre, edad, departamento);
        this.fechaAlta = fechaAlta;
        this.fechaBaja = fechaBaja;
        this.sueldoMensual = sueldoMensual;
    }
    
    public String getFechaAlta() {
        return fechaAlta;
    }

    public void setFechaAlta(String fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public String getFechaBaja() {
        return fechaBaja;
    }

    public void setFechaBaja(String fechaBaja) {
        this.fechaBaja = fechaBaja;
    }
    
    public Double getSueldoMensual() {
        return sueldoMensual;
    }
    
    public void setSueldoMensual(Double sueldoMensual) {
        this.sueldoMensual = sueldoMensual;
                
    }

    @Override
    public Double calcularSueldo() throws Exception {
        if(sueldoMensual == null) {
            throw new Exception("El empleado "+this.getNombre() + " no tiene asignado un sueldo mensual");
        }
        
        return this.sueldoMensual;
    }
   
    public String toString() {
        Boolean flagError = false;
        String string = super.toString();
        try {
            string += "Fecha de alta: " + this.getFechaAlta() + "\n";
            string += "Fecha de baja: " + this.getFechaBaja() + "\n";
            string += "Sueldo mensual: "+this.calcularSueldo();
            string += "\n";
            string += "####################";
        } catch(Exception ex){
            flagError = true;
        }
        finally {
            if(flagError) {
                string += "El sueldo mensual no se pudo determinar \n";
            }
        }
        
        return string;
    }
    
}
