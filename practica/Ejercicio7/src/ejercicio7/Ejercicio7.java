/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ejercicio7;

import java.util.ArrayList;
import java.util.Iterator;

/**
 *
 * @author dakota
 */
public class Ejercicio7 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        Alumno alumno = new Alumno("David", "Fichen", 316545, "id-12390");
        Alumno alumno1 = new Alumno("Juan", "Gonzalez", 326545, "id-12391");
        Profesor profesor = new Profesor("Pedro", "Juan", 1022123, Double.valueOf(7000));
        Profesor profesor2 = new Profesor("Favian", "Dominguez", 1123, Double.valueOf(3500));
        
        ArrayList<Persona> P = new ArrayList<>();
        
        P.add(alumno);
        P.add(alumno1);
        P.add(profesor);
        P.add(profesor2);
        Iterator<Persona> iP = P.iterator();
        
        while(iP.hasNext()) {
            Persona p = iP.next();
            String tipo = p.getClass().toString();
            tipo = tipo.replaceAll("class ejercicio7.", "");
            System.out.println("Datos de "+tipo);
            System.out.println(p);
            System.out.println("");
            
        }
        
    }
    
    
}
