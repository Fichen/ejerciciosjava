/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicio7;

/**
 * Abstracción derivada de Persona. Se agregan atributos propios
 *
 * @author dakota
 * @version 1.0
 */
public class Profesor extends Persona {

    private Double sueldo;
    
    public Profesor() {
        super();
    }
    
    /**
     * Inicializo los paramtros de la clase base y los propios también.
     * @param nombre String
     * @param apellido String
     * @param documento Integer
     * @param sueldo  Double
     */
    public Profesor(String nombre, String apellido, Integer documento, Double sueldo) {        
        super(nombre, apellido, documento);
        this.sueldo = sueldo;
    }
    
    /**
     * Materias que dicta el profesor
     */
    private enum EMaterias {
        MATEMATICA("Matematica"), FISICA("Fisica"), QUIMICA("Quimica");
        private String nombreMateria;
        
        EMaterias(String nombreMateria) {
            this.nombreMateria = nombreMateria;
        }

        @Override
        public String toString() {
            return this.nombreMateria;
        }
    }
    
    public Double getSueldo() {
        return this.sueldo;
    }
    
    public void setSueldo(Double sueldo) {
        this.sueldo = sueldo;
    }
    
    /**
     * Retorna en un único string todas las materias de los profesores.
     * @return materias String
     */
    public String getMaterias() {
        String materias = "";
        
        for (EMaterias EM: EMaterias.values()) {
            
            materias += EM + " - ";
        }
                        
        return materias.substring(0, materias.length()-2);
        
    }
    
    @Override
    public String toString() {
        String profesor = super.toString();
        profesor += "Sueldo: $"+this.sueldo +"\n";
        profesor += "Materias: "+this.getMaterias();
        return profesor;
    }
    

}
