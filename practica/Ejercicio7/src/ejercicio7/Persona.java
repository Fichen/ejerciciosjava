/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ejercicio7;

/**
 * Abstracción generica de una persona
 * @author dakota
 * @version 1.0
 */
public abstract class Persona {
    
    private String nombre;
    private String apellido;
    private Integer documento;
    
    /**
     * Constructor por defecto
     */
    public Persona() {        
    }
    
    /**
     * Inicializa los atributos nombre, apellido y documento
     * @param nombre String
     * @param apellido String
     * @param documento Integer
     */
    public Persona(String nombre, String apellido, Integer documento) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.documento = documento;
    }
    
    
    public String getNombre() {
        return this.nombre;
    }
    
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    public String getApellido() {
        return this.apellido;
    }
    
    public void setApellido(String apellido) {
        this.apellido = apellido;        
    }
    
    public Integer getDocumento() {
        return this.documento;
    }
    
    public void setDocumento(Integer documento) {
        this.documento = documento;
    }
    
    @Override
    public String toString() {
       String datos = "Nombre: "+this.nombre + "\n";
       datos += "Apellido: "+this.apellido + "\n";
       datos += "Documento: "+this.documento + "\n";
       return datos;        
    }
    
    
}
