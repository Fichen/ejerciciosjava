/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ejercicio7;

/**
 * Abstración derivada de persona. Agrego atributos
 * @author dakota
 * @version 1.0
 */
public class Alumno extends Persona{
    
    private String legajo;
    
    public Alumno() {
        super();
    }
    
    /**     
     * Inicialización del objeto con los parametros dados. También se inicializa
     * la clase base
     * @param nombre Sring
     * @param apellido String
     * @param documento Integer
     * @param legajo String
     */
    public Alumno(String nombre, String apellido, Integer documento, String legajo ) {
        super(nombre, apellido, documento);
        this.legajo = legajo;
    }
        
    @Override
    public String toString() {
        String alumno = super.toString();
        alumno += "Legajo: " + legajo;        
        return alumno;
    }
}
