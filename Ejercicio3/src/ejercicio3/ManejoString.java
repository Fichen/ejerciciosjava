package ejercicio3;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 *
 * @author alumno
 */
public class ManejoString {
    private String cadena;
    private int totalCaracteres;
    
   public void setCadena(String cadena) {
       this.cadena = cadena;
       this.totalCaracteres = this.cadena.length();
   }
     
    
    public String retornarMitadCadena() {
        return this.cadena.substring(0, totalCaracteres/2);
    }
    
    
    public String retornarUltimoCaracter() {
        return this.cadena.substring(totalCaracteres-1);
    }
    
    public String retornarInverso() {
        char[] inverso = this.cadena.toCharArray();
        String nuevaCadena ="";
        for(int i = this.totalCaracteres -1 ; i >= 0; i--) {
            nuevaCadena += inverso[i];
            
        }
        return nuevaCadena;
    }
    
    public String retornarConGuion() {
        char[] inverso = this.cadena.toCharArray();
        String nuevaCadena ="";
        for(int i = 0; i <= this.totalCaracteres -1; i++) {
            nuevaCadena += inverso[i] +"-";            
        }
        return nuevaCadena;
    }
    
    public int retornarVocales() {
        char[] inverso = this.cadena.toCharArray();
        
        int vocales = 0;
        for(int i = 0; i <= this.totalCaracteres -1; i++) {
            String letra = String.valueOf(inverso[i]);
            
            if(letra.matches("a|e|i|o|u")) {
                //System.out.println(letra.toString());
                vocales++;
            }
            else{
                //System.out.println("NO");
            }
        }
        return vocales;
    }
    
    public Boolean verificarCapicua() {
        char[] cadena = this.cadena.toLowerCase().toCharArray();
        String inverso = "";
        
        for(int i = this.totalCaracteres -1; i >= 0; i--) {
            inverso += cadena[i];
        }
        
        return this.cadena.equalsIgnoreCase(inverso);
        
    }
}
