/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicio3;

import java.util.Scanner;

/**
 *
 * @author alumno
 */
public class Ejercicio3 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner sc = new Scanner(System.in);
        
        System.out.println("Ingresar cadena");
        String cadenaIngresada = sc.nextLine();
        ManejoString ms = new ManejoString();
        ms.setCadena(cadenaIngresada);
        
        System.out.println("A) Mitad de caracteres: " + ms.retornarMitadCadena() );
        System.out.println("B) Ultimo caracter: " + ms.retornarUltimoCaracter());
        System.out.println("C) Inverso: " + ms.retornarInverso());
        System.out.println("D) Separado por guion: " + ms.retornarConGuion());
        System.out.println("E) vocales: " + ms.retornarVocales());
        if (ms.verificarCapicua()) {
            System.out.println("F) La cadena "+cadenaIngresada +" es capicua");
        }
        else {
            System.out.println("F) La cadena "+cadenaIngresada +" NO es capicua");
        }
        
        //String
        
        
        
        
        
    }
    
}
