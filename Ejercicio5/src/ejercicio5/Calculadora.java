package ejercicio5;

import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author alumno
 */
public class Calculadora implements ICalcular {
    
    public Double sumar(Number num1, Number num2) {
        
        return num1.doubleValue() + num1.doubleValue();
    }
    
    public Double restar(Number num1, Number num2) {
        
        return num1.doubleValue() - num2.doubleValue();
    }
    
    public Double multiplicar(Number num1, Number num2) {
        
        return num1.doubleValue() * num2.doubleValue();
    }
    
    public Double dividir(Number num1, Number num2) {
        
        if (num2.intValue() == 0) {
            throw new IllegalArgumentException("No se puede dividir por cero");
        }
        
        return num1.doubleValue() / num2.doubleValue();
                
    }
    
    
}
