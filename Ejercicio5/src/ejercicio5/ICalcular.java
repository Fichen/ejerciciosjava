package ejercicio5;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Contrato de operaciones para una Calculadora
 * @author alumno
 * @version 1.0
 */
public abstract interface ICalcular {
    
    /**
     * Suma el num1 con el num2
     * @param num1 Number
     * @param num2 Number
     * @return Double
     */
    public abstract Double sumar(Number num1, Number num2);
    
     /**
     * Diferencia entre el num1 con el num2
     * @param num1 Number
     * @param num2 Number
     * @return Double
     */
    public abstract Double restar(Number num1, Number num2);
    
     /**
     * Divide dos numeros
     * @param num1 Number : numero dividendo
     * @param num2 Number : numero divisor
     * @return Double
     */
    public abstract Double dividir(Number num1, Number num2);
    
     /**
     * Multiplica dos numeros
     * @param num1 Number
     * @param num2 Number
     * @return Double
     */
    public abstract Double multiplicar(Number num1, Number num2);
}
