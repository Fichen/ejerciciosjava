/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicio5;

import java.util.Scanner;

/**
 *
 * @author alumno
 */
public class Ejercicio5 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Calculadora c;
        Scanner sc = new Scanner(System.in);
        Integer op = 0;
        Double num1, num2, rst = 0.0;
        String operacion = "";
        
        try {
            c = new Calculadora();
            do {                           
                System.out.println("Ingrese el número de la operación que quiere realizar:");
                System.out.println("1 -> Sumar");
                System.out.println("2 -> Restar");
                System.out.println("3 -> Multiplicar");
                System.out.println("4 -> Dividir");
                operacion = sc.nextLine();                
                
                if(!operacion.matches("[1-4]")) {
                    op = 0;
                }
                else{
                    op = Integer.valueOf(operacion);
                }
                
                for (int clear = 0; clear < 500; clear++) {
                    System.out.println("\b");
                }
                
            } while (op == 0);                        
                    
            System.out.println("Ingrese un numero:");
            num1 = Double.valueOf(sc.nextLine());
            System.out.println("Ingrese otro numero:");
            num2 = Double.valueOf(sc.nextLine());
            switch (op) {
                case 1:
                    rst = c.sumar(num1, num2);
                    operacion = "sumar";
                    break;
                case 2:
                    rst = c.restar(num1, num2);
                    operacion = "restar";
                    break;
                case 3:
                    rst = c.multiplicar(num1, num2);
                    operacion = "multiplicar";
                    break;
                case 4:
                    rst = c.dividir(num1, num2);
                    operacion = "dividir";
                    break;                    
                default:
                    break;
            }
            
            System.out.println("El resultado de "+operacion+" "+num1 +" con "+ num2 + " es "+rst);
            
        } catch (Exception e) {
            
        }
    }
}
